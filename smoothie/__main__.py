if __name__ == "__main__":
    print(
        """
███████ ███    ███  ██████   ██████  ████████ ██   ██ ██ ███████     ████████ ██ ███    ███ ███████ ██ 
██      ████  ████ ██    ██ ██    ██    ██    ██   ██ ██ ██             ██    ██ ████  ████ ██      ██ 
███████ ██ ████ ██ ██    ██ ██    ██    ██    ███████ ██ █████          ██    ██ ██ ████ ██ █████   ██ 
     ██ ██  ██  ██ ██    ██ ██    ██    ██    ██   ██ ██ ██             ██    ██ ██  ██  ██ ██         
███████ ██      ██  ██████   ██████     ██    ██   ██ ██ ███████        ██    ██ ██      ██ ███████ ██ 
"""
    )
    print(
        "Welcome to this awesome tool. It creates nice grocey lists based on your smoothie selection!"
    )

