from typing import Union

SI_PREFIXES = {
    "k": ("kilo", 3),
    "h": ("hecto", 2),
    "da": ("deca", 1),
    "": ("", 0),
    "d": ("deci", -1),
    "c": ("centi", -2),
    "m": ("milli", -3),
}


class UnitPrefix:
    def __init__(self, symbol):
        self.symbol = symbol
        try:
            (self.name, self.power) = SI_PREFIXES[symbol]
        except KeyError:
            self.symbol = None
            self.name = None
            self.power = None
            raise ValueError(
                "Provided symbol '{}' appears not to be a SI prefix".format(symbol)
            )

    def __repr__(self):
        return "{}({})".format(
            self.__class__.__name__,
            ", ".join([str(v) for (k, v) in self.__dict__.items()]),
        )

    def __eq__(self, other):
        return all([other.__dict__.get(k) == v for (k, v) in self.__dict__.items()])


SI_UNITS = {"l": "liter", "g": "gram", "qty": "Quantity"}


class Unit:
    """It all starts with a unit. Preferably, SI please"""

    def __init__(self, symbol):
        # Find out if the symbol is a possible combination of prefix + unit
        def decompose_symbol(symbol: str) -> (str, str):
            for (prefix, _) in SI_PREFIXES.items():
                for (unit, _) in SI_UNITS.items():
                    if (prefix + unit) == symbol:
                        return (prefix, unit)
            raise ValueError(
                "Provided symbol '{}' appears not to be a valid unit".format(symbol)
            )

        (prefix, self.unit_symbol) = decompose_symbol(symbol)
        self.name = SI_UNITS[self.unit_symbol]
        self.prefix = UnitPrefix(prefix)

    def factor(self):
        return 10 ** self.prefix.power

    def __repr__(self):
        return "{}({})".format(
            self.__class__.__name__,
            ", ".join([str(v) for (k, v) in self.__dict__.items()]),
        )

    def __eq__(self, other):
        return all([other.__dict__.get(k) == v for (k, v) in self.__dict__.items()])


class Quantity:
    """Quantity consists of a amount with a unit to unambigously construct a quantity"""

    def __init__(self, amount: int, unit: str):
        self.amount = amount
        self.unit = Unit(unit)

    def base(self):
        return self.amount * self.unit.factor()

    def __repr__(self):
        return "{}({}, {})".format(self.__class__.__name__, self.amount, self.unit)

    def __eq__(self, other):
        return self.base() == other.base()

    def __lt__(self, other):
        return self.base() < other.base()

    def __le__(self, other):
        return (self < other) or (self == other)

    def __gt__(self, other):
        return not (self <= other)

    def __ge__(self, other):
        return not (self < other)

    def __add__(self, other):
        if self.unit.unit_symbol != other.unit.unit_symbol:
            raise TypeError(
                "Unsupported operand type(s) for +: {}('{}') and {}('{}')".format(
                    self.__class__.__name__,
                    self.unit.name,
                    other.__class__.__name__,
                    other.unit.name,
                )
            )

        sum = self.base() + other.base()
        return Quantity(sum, "g")


def parse_quantity_string(qty: str) -> (Union[int, float], str):
    components = qty.split()

    try:
        quantity = int(components[0])
    except ValueError:
        quantity = float(components[0])

    if len(components) == 1:
        unit = ""
    else:
        unit = components[1]

    return (quantity, unit)



class Ingredient:
    """A ingredient must be healthy, but sometimes it isn't"""

    def __init__(self, name: str, description: str, qty: str):
        self.name = name
        self.description = description
        # self.quantity = Quantity()


class Smoothie:
    """Smoothie is aggegration of ingredients that will put in the blender eventually."""

    def __init__(self):
        self.name = ""
        self.description = ""
        self.ingredients = {}
        self.rating = None
