import unittest
import smoothie


class testIngredientClass(unittest.TestCase):
    def test_parse_quantity_string_with_unit(self):
        self.assertEqual(smoothie.parse_quantity_string("1 kg"), (1, "kg"))

    def test_parese_quantity_string_without_unit(self):
        self.assertEqual(smoothie.parse_quantity_string("1"), (1, ""))

    def test_parse_floating_point_number(self):
        self.assertEqual(smoothie.parse_quantity_string("1.2 kg"), (1.2, "kg"))

    @unittest.SkipTest
    def test_parse_quantity_string_unit_only(self):
        with self.assertRaises(ValueError):
            smoothie.parse_quantity_string("kg")

    # Throw on invalid casese (1kg, kg1, kg 1)


class testSmoothieClass(unittest.TestCase):
    pass
