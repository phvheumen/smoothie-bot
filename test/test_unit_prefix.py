import unittest
import smoothie


class testUnitPrefixClass(unittest.TestCase):
    def test_valid_instantiation(self):
        prefix = smoothie.UnitPrefix("k")
        self.assertEqual(prefix.name, "kilo")
        self.assertEqual(prefix.symbol, "k")
        self.assertEqual(prefix.power, 3)

    def test_invalid_symbol_throw(self):
        with self.assertRaises(ValueError):
            smoothie.UnitPrefix("i")
