import unittest
import smoothie


class testQuantityClass(unittest.TestCase):
    def test_normal_instantiation(self):
        obj = smoothie.Quantity(1, "kg")
        self.assertEqual(obj.amount, 1)
        self.assertEqual(obj.unit, smoothie.Unit("kg"))

    def test_invalid_unit_instatiation(self):
        with self.assertRaises(ValueError):
            obj = smoothie.Quantity(1, "Mohm")

    def test_equal_units_different_prefix(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(1000, "g")
        self.assertEqual(obj1, obj2)

    def test_unequal_units(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(500, "g")
        self.assertNotEqual(obj1, obj2)

    def test_less_than(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(500, "g")
        self.assertLess(obj2, obj1)

    def test_less_equal_than1(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(1000, "g")
        self.assertLessEqual(obj2, obj1)

    def test_less_equal_than2(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(500, "g")
        self.assertLessEqual(obj2, obj1)

    def test_greater_than(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(500, "g")
        self.assertGreater(obj1, obj2)

    def test_greater_equal_than1(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(500, "g")
        self.assertGreaterEqual(obj1, obj2)

    def test_greater_equal_than2(self):
        obj1 = smoothie.Quantity(1, "kg")
        obj2 = smoothie.Quantity(1000, "g")
        self.assertGreaterEqual(obj1, obj2)

    def test_add1(self):
        obj = smoothie.Quantity(100, "g") + smoothie.Quantity(100, "g")
        self.assertEqual(obj, smoothie.Quantity(200, "g"))

    def test_add2(self):
        obj = smoothie.Quantity(600, "g") + smoothie.Quantity(600, "g")
        self.assertEqual(obj, smoothie.Quantity(1.2, "kg"))

    def test_add3(self):
        with self.assertRaises(TypeError):
            obj = smoothie.Quantity(600, "g") + smoothie.Quantity(300, "ml")

    # Test throw compare incompatible units
    # Test arithmatic (+, -, *, /)
    # Arithmatic with plain number in one of the arguments
    # Disallow negative quantities
