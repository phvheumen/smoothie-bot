import unittest
import smoothie


class testUnitClass(unittest.TestCase):
    def test_instantiation_without_prefx(self):
        obj = smoothie.Unit("l")
        self.assertEqual(obj.prefix, smoothie.UnitPrefix(""))
        self.assertEqual(obj.unit_symbol, "l")
        self.assertEqual(obj.name, "liter")

    def test_instantiation_with_prefix(self):
        obj = smoothie.Unit("kg")
        self.assertEqual(obj.prefix, smoothie.UnitPrefix("k"))
        self.assertEqual(obj.unit_symbol, "g")
        self.assertEqual(obj.name, "gram")

    def test_instantiation_with_double_letter_prefix(self):
        obj = smoothie.Unit("dal")
        self.assertEqual(obj.prefix, smoothie.UnitPrefix("da"))
        self.assertEqual(obj.unit_symbol, "l")

    def test_instantiation_with_invalid_unit(self):
        with self.assertRaises(ValueError):
            smoothie.Unit("MOhm")

    def test_throw_on_prefix_only(self):
        with self.assertRaises(ValueError):
            smoothie.Unit("k")
